// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity >=0.5.0;

interface IWQ {
    function name() external view returns (string memory);

    function symbol() external view returns (string memory);

    function decimals() external view returns (uint8);

    function totalSupply() external view returns (uint256);

    function deposit() external payable;

    function transfer(address to, uint256 value) external returns (bool);

    function transferFrom(
        address src,
        address dst,
        uint256 wad
    ) external returns (bool);

    function approve(address guy, uint256 wad) external returns (bool);

    function withdraw(uint256) external;

    function balanceOf(address) external view returns (uint256);

    function allowance(address, address) external view returns (uint256);
}
