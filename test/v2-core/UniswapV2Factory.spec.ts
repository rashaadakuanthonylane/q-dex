import chai, { expect } from "chai";
import { BigNumber } from "ethers";
import { solidity } from "ethereum-waffle";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

import { getCreate2Address } from "./shared/utilities";
import {
  UniswapV2Factory,
  UniswapV2Pair__factory,
  ContractRegistry,
} from "../../typechain";
import { keccak256 } from "ethers/lib/utils";

chai.use(solidity);

const DEX_PARAMETERS_VOTING_KEY =
  "governance.experts.EPDR.QDEX.parametersVoting";

const TEST_ADDRESSES: [string, string] = [
  "0x1000000000000000000000000000000000000000",
  "0x2000000000000000000000000000000000000000",
];

describe("UniswapV2Factory", () => {
  let wallet: SignerWithAddress, other: SignerWithAddress;

  let factory: UniswapV2Factory;
  let registry: ContractRegistry;
  let UniswapV2Pair: UniswapV2Pair__factory;

  beforeEach(async () => {
    [wallet, other] = await ethers.getSigners();

    const registryFactory = await ethers.getContractFactory("ContractRegistry");
    registry = await registryFactory.deploy();
    await registry.initialize([wallet.address], [], []);

    const Factory = await ethers.getContractFactory("UniswapV2Factory");
    factory = await Factory.deploy(registry.address);

    UniswapV2Pair = await ethers.getContractFactory("UniswapV2Pair");
  });

  async function createPair(tokens: [string, string]) {
    const bytecode = UniswapV2Pair.bytecode;
    const create2Address = getCreate2Address(factory.address, tokens, bytecode);
    await expect(factory.createPair(...tokens))
      .to.emit(factory, "PairCreated")
      .withArgs(
        TEST_ADDRESSES[0],
        TEST_ADDRESSES[1],
        create2Address,
        BigNumber.from(1)
      );

    await expect(factory.createPair(...tokens)).to.be.reverted; // UniswapV2: PAIR_EXISTS
    const [tokenARev, tokenBRev] = tokens.reverse();
    await expect(factory.createPair(tokenARev, tokenBRev)).to.be.reverted; // UniswapV2: PAIR_EXISTS
    expect(await factory.getPair(...tokens)).to.eq(create2Address);
    expect(await factory.getPair(tokenARev, tokenBRev)).to.eq(create2Address);
    expect(await factory.allPairs(0)).to.eq(create2Address);
    expect(await factory.allPairsLength()).to.eq(1);

    const pair = await ethers.getContractAt(
      "UniswapV2Pair",
      await factory.getPair(TEST_ADDRESSES[0], TEST_ADDRESSES[1])
    );

    expect(await pair.factory()).to.eq(factory.address);
    expect(await pair.token1()).to.eq(TEST_ADDRESSES[0]);
    expect(await pair.token0()).to.eq(TEST_ADDRESSES[1]);
  }

  it("createPair", async () => {
    await createPair(TEST_ADDRESSES);
  });

  it("createPair:reverse", async () => {
    await createPair(TEST_ADDRESSES.reverse() as [string, string]);
  });

  it("createPair:gas", async () => {
    const tx = await factory.createPair(...TEST_ADDRESSES);
    const receipt = await tx.wait();
    expect(receipt.gasUsed).to.eq(2327034); // Gas used to be 3639401...
  });

  it("toggleFlatFee:denied", async () => {
    await registry.setAddress(DEX_PARAMETERS_VOTING_KEY, other.address);

    const bytecode = UniswapV2Pair.bytecode;
    const pair = getCreate2Address(factory.address, TEST_ADDRESSES, bytecode);
    await expect(factory.toggleFlatFee(pair)).to.be.revertedWith(
      "[QDEX-002000]-Permission denied - only governance voting have access."
    );
  });

  it("toggleFlatFee:successful", async () => {
    registry.setAddress(DEX_PARAMETERS_VOTING_KEY, wallet.address);

    const bytecode = UniswapV2Pair.bytecode;
    const pair = getCreate2Address(factory.address, TEST_ADDRESSES, bytecode);
    expect(await factory.checkForFlatFee(pair)).eq(true);

    expect(factory.toggleFlatFee(pair))
      .to.emit(factory, "FlatFeeToggled")
      .withArgs(pair, false);
    expect(await factory.checkForFlatFee(pair)).eq(false);

    expect(factory.toggleFlatFee(pair))
      .to.emit(factory, "FlatFeeToggled")
      .withArgs(pair, true);
    expect(await factory.checkForFlatFee(pair)).eq(true);
  });

  it("initHash", async () => {
    const bytecode = UniswapV2Pair.bytecode;
    expect(await factory.initHash()).eq(keccak256(bytecode));
  });

  /** feeTo is now fetched dynamically from the registry
  it("setFeeTo", async () => {
    await expect(
      factory.connect(other).setFeeTo(other.address)
    ).to.be.revertedWith("UniswapV2: FORBIDDEN");
    await factory.setFeeTo(wallet.address);
    expect(await factory.feeTo()).to.eq(wallet.address);
  });

  it("setFeeToSetter", async () => {
    await expect(
      factory.connect(other).setFeeToSetter(other.address)
    ).to.be.revertedWith("UniswapV2: FORBIDDEN");
    await factory.setFeeToSetter(other.address);
    expect(await factory.feeToSetter()).to.eq(other.address);
    await expect(factory.setFeeToSetter(wallet.address)).to.be.revertedWith(
      "UniswapV2: FORBIDDEN"
    );
  });
   */
});
