import { ethers } from "hardhat";

const WQAddress = "0xBb1709fa271Ed5071269Fa6d349f02Ac184189a3";
const contractRegistryAddress = "0xc3E589056Ece16BCB88c6f9318e9a7343b663522";

const token1Address = "0xBb1709fa271Ed5071269Fa6d349f02Ac184189a3";
const token2Address = "0x638ff23184c925C09d2E37F228888264eA888BCc";

async function main() {
  const [wallet] = await ethers.getSigners();

  const ContractRegistry = await ethers.getContractFactory("ContractRegistry");
  const contractRegistry = await ContractRegistry.attach(
    contractRegistryAddress
  );

  const Factory = await ethers.getContractFactory("UniswapV2Factory");
  const factory = await Factory.deploy(contractRegistryAddress);
  await factory.deployed();
  await contractRegistry.setAddress(
    "governance.intApp.QDEX.factory",
    factory.address
  );

  const Router = await ethers.getContractFactory("UniswapV2Router02");
  const router = await Router.deploy(
    factory.address,
    WQAddress,
    contractRegistryAddress
  );
  await router.deployed();
  await contractRegistry.setAddress(
    "governance.intApp.QDEX.router",
    router.address
  );

  console.log("Factory:", factory.address);
  console.log("Router:", router.address);

  await contractRegistry.setAddress("defi.WQ.coin", WQAddress);

  const ERC20Factory = await ethers.getContractFactory("UniswapV2ERC20");
  const token1 = await ERC20Factory.attach(token1Address);
  const token2 = await ERC20Factory.attach(token2Address);

  await token1.approve(router.address, "9999999999999999999999");
  await token2.approve(router.address, "9999999999999999999999");

  // Add liquidity
  // wallet.sendTransaction({
  //   to: router.address,
  //   data: "0xe8e33700000000000000000000000000bb1709fa271ed5071269fa6d349f02ac184189a3000000000000000000000000638ff23184c925c09d2e37f228888264ea888bcc0000000000000000000000000000000000000000000000000de0b6b3a76400000000000000000000000000000000000000000000000000000000000005f5e10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000868ac58d30f7d9043fa3994a3339d671d920ffd000000000000000000000000000000000000000000000021e19e0c9bab23fffff",
  // });
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
